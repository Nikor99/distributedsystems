package com.example;

import io.micronaut.http.HttpStatus;
import io.micronaut.http.annotation.*;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Controller("/items")
public class Databaseputter {
    private static final String DB_URL = "jdbc:postgresql://localhost:5432/tododb";
    private static final String USERNAME = "myuser";
    private static final String PASSWORD = "postgres";
    @Post
    public void insertItem(String todo, int priority) {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet generatedKeys = null;
        try {
            // Open a connection to the database
            conn = DriverManager.getConnection(DB_URL, USERNAME, PASSWORD);

            // Prepare the SQL insert statement
            String sql = "INSERT INTO todo (todo, priority) VALUES (?,?)";
            stmt = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

            // Set the values for the prepared statement
            stmt.setString(1, todo);
            stmt.setInt(2, priority);

            // Execute the insert statement
            stmt.executeUpdate();
            generatedKeys = stmt.getGeneratedKeys();
            if (generatedKeys.next()) {
                // Retrieve the generated ID
                int generatedId = generatedKeys.getInt(1);
                System.out.println("Generated ID: " + generatedId);
            }
        } catch (SQLException e) {
            // Handle any errors that may have occurred
            e.printStackTrace();
        } finally {
            // Close the resources
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException e) {
                    // Ignore
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    // Ignore
                }
            }
        }
    }
    @Put("{id}")
    public void updateItem(int id, String todo, int priority) {
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            // Open a connection to the database
            conn = DriverManager.getConnection(DB_URL, USERNAME, PASSWORD);

            // Prepare the SQL update statement
            String sql = "UPDATE todo SET todo = ?, priority = ? WHERE id = ?";
            stmt = conn.prepareStatement(sql);

            // Set the values for the prepared statement
            stmt.setString(1, todo);
            stmt.setInt(2, priority);
            stmt.setInt(3, id);

            // Execute the update statement
            stmt.executeUpdate();
        } catch (SQLException e) {
            // Handle any errors that may have occurred
            e.printStackTrace();
        } finally {
            // Close the resources
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException e) {
                    // Ignore
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    // Ignore
                }
            }
        }
    }
    @Get("/todos")
    public List<TodoItem> getTodos() throws SQLException {
        // Establish a connection to the database
        Connection conn = null;
        conn = DriverManager.getConnection(DB_URL, USERNAME, PASSWORD);

        // Create a PreparedStatement to execute the SQL query
        PreparedStatement stmt = conn.prepareStatement("SELECT id, todo, priority FROM todo");

        // Execute the query and retrieve the results
        ResultSet rs = stmt.executeQuery();

        // Iterate over the results and add each one to the list of todos
        List<TodoItem> todos = new ArrayList<>();
        while (rs.next()) {
            int id = rs.getInt("id");
            String todo = rs.getString("todo");
            int priority = rs.getInt("priority");
            todos.add(new TodoItem(id, todo, priority));
        }

        return todos;
    }

    @Delete("/todo/{id}")
    public void deleteTodo(int id) {
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            // Open a connection to the database
            conn = DriverManager.getConnection(DB_URL, USERNAME, PASSWORD);

            // Prepare the SQL insert statement
            String sql = "DELETE FROM todo WHERE id = ?";
            stmt = conn.prepareStatement(sql);

            // Set the values for the prepared statement
            stmt.setInt(1, id);

            // Execute the insert statement
            stmt.executeUpdate();
        } catch (SQLException e) {
            // Handle any errors that may have occurred
            e.printStackTrace();
        } finally {
            // Close the resources
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException e) {
                    // Ignore
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    // Ignore
                }
            }
        }
    }
}
