package com.example;

public class TodoItem {
    private int id;
    private String todo;
    private int priority = 2;

    public TodoItem(){

    }
    public TodoItem(String todo){
        this.todo = todo;
    }
    public TodoItem(int id, String todo, int priority){
        this.id = id;
        this.todo = todo;
        this.priority = priority;
    }

    public void setTodo(String todo) {
        this.todo = todo;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public String getTodo() {
        return todo;
    }

    public int getPriority() {
        return priority;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
