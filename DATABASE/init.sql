 CREATE TABLE todo (
    id SERIAL PRIMARY KEY,
    todo character varying(500) NULL,
    priority integer NULL
);
